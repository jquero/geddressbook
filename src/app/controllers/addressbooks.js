var Addressbooks = function() {
	this.respondsWith = ['html', 'json', 'xml', 'js', 'txt'];

	this.add = function(req, resp, params) {
		this.respond({ params: params });
	};

	this.create = function(req, resp, params) {
		var self = this,
			addressbook = geddy.model.Addressbook.create(params);

		if (!addressbook.isValid()) {
			this.respondWith(addressbook);
		} else {
			addressbook.save(function(err, data) {
				if (err) { throw err; }
				self.respondWith(addressbook, { status: err });
			});
		}
	};

	this.show = function(req, resp, params) {
		var self = this;

		geddy.model.Addressbook.all(function(err, data) {
			if (err) { throw err; }
			self.respond({ params: params, addressbooks: data }, 
				{ format: 'html', template: 'app/views/main/index' });
		});
	};

	this.edit = function(req, resp, params) {
		var self = this;

		geddy.model.Addressbook.first(params.id, function(err, addressbook) {
			if (err) { throw err; }
			if (!addressbook) {
				throw new geddy.errors.BadRequestError();
			} else {
				self.respondWith(addressbook);
			}
		});
	};

	this.update = function(req, resp, params) {
		var self = this;

		geddy.model.Addressbook.first(params.id, function(err, addressbook) {
			if (err) { throw err; }
			addressbook.updateProperties(params);

			if (!addressbook.isValid()) {
				self.respondWith(addressbook);
			} else {
				addressbook.save(function(err, data) {
					if (err) { throw err; }
					self.respondWith(addressbook, { status: err });
				});
			}
		});
	};

	this.remove = function(req, resp, params) {
		var self = this;

		geddy.model.Addressbook.first(params.id, function(err, addressbook) {
			if (err) { throw err; }
			if (!addressbook) {
				throw new geddy.errors.BadRequestError();
			} else {
				geddy.model.Addressbook.remove(params.id, function(err) {
					if (err) { throw err; }
					self.respondWith(addressbook);
				});
			}
		});
	};

};

exports.Addressbooks = Addressbooks;