var Addressbook = function () {

  this.defineProperties({
    surname: {type: 'string', required: true},
    lastname: {type: 'string', required: true},
    phone: {type: 'string'},
    mobile: {type: 'string'},
    fax: {type: 'string'},
    email: {type: 'string'},
    address: {type: 'string'},
    zip: {type: 'string'},
    city: {type: 'string'},
    state: {type: 'string'},
    country: {type: 'string'}
  });

  /*
  this.property('login', 'string', {required: true});
  this.property('password', 'string', {required: true});
  this.property('lastName', 'string');
  this.property('firstName', 'string');

  this.validatesPresent('login');
  this.validatesFormat('login', /[a-z]+/, {message: 'Subdivisions!'});
  this.validatesLength('login', {min: 3});
  // Use with the name of the other parameter to compare with
  this.validatesConfirmed('password', 'confirmPassword');
  // Use with any function that returns a Boolean
  this.validatesWithFunction('password', function (s) {
      return s.length > 0;
  });

  // Can define methods for instances like this
  this.someMethod = function () {
    // Do some stuff
  };
  */

};

/*
// Can also define them on the prototype
Addressbook.prototype.someOtherMethod = function () {
  // Do some other stuff
};
// Can also define static methods and properties
Addressbook.someStaticMethod = function () {
  // Do some other stuff
};
Addressbook.someStaticProperty = 'YYZ';
*/

Addressbook = geddy.model.register('Addressbook', Addressbook);
