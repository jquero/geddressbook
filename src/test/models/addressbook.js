var assert = require('assert')
  , tests
  , Addressbook = geddy.model.Addressbook;

tests = {

  'after': function (next) {
    // cleanup DB
    Addressbook.remove({}, function (err, data) {
      if (err) { throw err; }
      next();
    });
  }

, 'simple test if the model saves without a error': function (next) {
    var addressbook = Addressbook.create({});
    addressbook.save(function (err, data) {
      assert.equal(err, null);
      next();
    });
  }

, 'test stub, replace with your own passing test': function () {
    assert.equal(true, false);
  }

};

module.exports = tests;
