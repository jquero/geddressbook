var CreateAddressbooks = function () {
  this.up = function (next) {
    var def = function (t) {
          t.column('surname', 'string');
          t.column('lastname', 'string');
          t.column('phone', 'string');
          t.column('mobile', 'string');
          t.column('fax', 'string');
          t.column('email', 'string');
          t.column('address', 'string');
          t.column('zip', 'string');
          t.column('city', 'string');
          t.column('state', 'string');
          t.column('country', 'string');
        }
      , callback = function (err, data) {
          if (err) {
            throw err;
          }
          else {
            next();
          }
        };
    this.createTable('addressbook', def, callback);
  };

  this.down = function (next) {
    var callback = function (err, data) {
          if (err) {
            throw err;
          }
          else {
            next();
          }
        };
    this.dropTable('addressbook', callback);
  };
};

exports.CreateAddressbooks = CreateAddressbooks;
